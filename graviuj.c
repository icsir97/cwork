#include<stdio.h>
#include<allegro5/allegro.h>
#include<allegro5/allegro_primitives.h>
#include<math.h>

    double CsE=149597870700;
    double nap=1.99*pow(10,30);
    double fold=5.97*pow(10,24);

    const double grav=6.67*pow(10,-11);


int main(){

    double gx=CsE,gy=0;
    double gvx=0,gvy=1000;

    double ax=0,ay=0;

    double tav,ero;
    al_init();
    al_init_primitives_addon();

    ALLEGRO_DISPLAY* kepernyo;
    kepernyo = al_create_display(1280,1024);
//---------------------------------------

    double i=0;

    while(1)
        {

            //al_clear_to_color(al_map_rgb(0,0,0));

            tav = sqrt(pow(ax-gx,2) + pow(ay-gy,2));
            ero = grav *(nap*fold / pow(tav,2));

            gvx+=ero*((ax-gx)/tav);
            gvy+=ero*((ay-gy)/tav);


            gx += gvx;
            gy += gvy;

            al_draw_filled_circle(ax,ay,20,al_map_rgb(255,0,0));
            al_draw_filled_circle(gx,gy,7,al_map_rgb(0,255,0));

            i++;
            if ((int)i%100==1){printf("x=%lf,y=%lf\n",gx,gy);}
            al_flip_display();
        }


//---------------------------------------
    al_flip_display();
    al_rest(10);
    al_destroy_display(kepernyo);

return 0;
}
