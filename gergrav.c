#include<stdio.h>
#include<allegro5/allegro.h>
#include<allegro5/allegro_primitives.h>
#include<math.h>

int main(){

    double x=555,y=420;
    double vx=-1,vy=0;

    double gx=555,gy=444;
    double gvx=0.7,gvy=0;

    double ax=555,ay=555;

    double tav,ero;
    al_init();
    al_init_primitives_addon();

    ALLEGRO_DISPLAY* kepernyo;
    kepernyo = al_create_display(1280,1024);
//---------------------------------------

    while(1)
        {

            al_clear_to_color(al_map_rgb(0,0,0));

            tav = sqrt(pow(gx-x,2) + pow(gy-y,2));
            ero = 50*(1 / pow(tav,2));

            vx+=ero*((gx-x)/tav);
            vy+=ero*((gy-y)/tav);


            x += vx;
            y += vy;


            tav = sqrt(pow(ax-gx,2) + pow(ay-gy,2));
            ero = 50*(1 / pow(tav,2));

            gvx+=ero*((ax-gx)/tav);
            gvy+=ero*((ay-gy)/tav);


            gx += gvx;
            gy += gvy;

            al_draw_filled_circle(ax,ay,20,al_map_rgb(255,0,0));
            al_draw_filled_circle(gx,gy,10,al_map_rgb(0,255,0));
            al_draw_filled_circle(x,y,5,al_map_rgb(0,0,255));
            al_flip_display();
        }


//---------------------------------------
    al_flip_display();
    al_rest(10);
    al_destroy_display(kepernyo);

return 0;
}
