
#include <stdio.h>
#include <allegro5/allegro.h>
#include<allegro5/allegro_primitives.h>

int main(){

    //init
    al_init();
    al_init_primitives_addon();

    //letrehozas

    ALLEGRO_DISPLAY* kepernyo;
    kepernyo = al_create_display(1280,1024);

    double x=450,y=350;
    double gx=640, gy=512;
    double vx=0.1,vy=1;
    double tav,ero;

    //rajz
    while(1){
          //  al_clear_to_color(al_map_rgb(0,0,0));

    tav = sqrt( pow(gx-x,2) + pow(gy-y,2));
    //ero = 5 / pow(sqrt(tav),3);
    ero = 500 / pow(tav,2);
    vx += (gx-x) * ero / tav;
    vy += (gy-y) * ero / tav;

    x += vx;
    y += vy;

    al_draw_filled_circle(x,y,5,al_map_rgb(0,122,255));
    al_draw_filled_circle(gx,gy,25,al_map_rgb(255,122,0));

    al_flip_display();

    }
    //megjelenites

    al_flip_display();
    al_rest(10);
    al_destroy_display(kepernyo);

return 0;
}
