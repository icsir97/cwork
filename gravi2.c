
#include <stdio.h>
#include <allegro5/allegro.h>
#include<allegro5/allegro_primitives.h>

int main(){

    //init
    al_init();
    al_init_primitives_addon();

    //letrehozas

    ALLEGRO_DISPLAY* kepernyo;
    kepernyo = al_create_display(1280,980);

    double x=450,y=350;
    double ax=640,ay=512;
    double gx=740, gy=350;
    double vx=0.5,vy=0.3;
    double vxx=-0.5, vyy=0.2;
    double tav,ero;
    double tav2,ero2;

    //rajz
    while(1){
            //al_clear_to_color(al_map_rgb(0,0,0));

    tav2 = sqrt( pow(ax-gx,2) + pow(ay-gy,2));
    //ero2 = 5 / pow(sqrt(tav2),3);
    ero2 = 50/pow(tav2,2);
    vxx += (ax-gx) * ero / tav2;
    vyy += (ay-gy) * ero / tav2;

    gx += vxx;
    gy += vyy;

    tav = sqrt( pow(gx-x,2) + pow(gy-y,2));
    //ero = 5 / pow(sqrt(tav),3);
    ero = 50 / pow(tav,2);
    vx += (gx-x)/(gx-x) * ero / tav;
    vy += (gy-y)/(gy-y) * ero / tav;

    x += vx;
    y += vy;

    al_draw_filled_circle(x,y,5,al_map_rgb(0,255,0));
    al_draw_filled_circle(gx,gy,10,al_map_rgb(0,0,255));
    al_draw_filled_circle(ax,ay,15,al_map_rgb(255,0,0));


    al_flip_display();
    al_rest(0.0001);

    }
    //megjelenites

    al_flip_display();
    al_rest(10);
    al_destroy_display(kepernyo);

return 0;
}
